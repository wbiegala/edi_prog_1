﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_1
{
    class Pesel : Input, IAnalyzer
    {
        public Pesel(string val) : base(val)
        {

        }


        public override bool Analyze()
        {
            if (Int64.TryParse(this.value, out Int64 result) == false)
                throw new StringNotValidException("PESEL powinien składać się tylko z cyfr.");

            if (this.value.Length != 11)
                throw new StringNotValidException("PESEL powinien składać się z 11 cyfr.");

            int[] importances = { 9, 7, 3, 1, 9, 7, 3, 1, 9, 7, 0 };
            int sum = this.value.Zip(importances, (digit, importance) => (digit - '0') * importance).Sum();

            if (sum % 10 != this.value[10] - '0')
                throw new StringNotValidException("PESEL ma błędną sumę kontrolną.");

            return true;
        }
    }
}
