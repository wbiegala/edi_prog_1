﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_1
{
    public class StringNotValidException : ApplicationException
    {
        private string description;

        public string Description { get => description; }

        public StringNotValidException(string desc)
        {
            this.description = desc;
        }
    }
}