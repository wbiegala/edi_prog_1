﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_1
{
    class Nip : Input, IAnalyzer
    {
        public Nip(string val) : base(val)
        {
        }

        public override bool Analyze()
        {
            if (Int64.TryParse(this.value, out Int64 result) == false)
                throw new StringNotValidException("NIP powinien składać się tylko z cyfr.");

            if (this.value.Length != 10)
                throw new StringNotValidException("NIP powinien składać się z 10 cyfr.");


            int[] importances = { 6, 5, 7, 2, 3, 4, 5, 6, 7, 0 };
            int sum = this.value.Zip(importances, (digit, importance) => (digit - '0') * importance).Sum();

            if (sum % 11 != this.value[9] - '0')
                throw new StringNotValidException("NIP ma błędną sumę kontrolną.");

            return true;
        }
    }
}
