﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace prog_1
{
    public partial class MainWindow : Form
    {
        IAnalyzer analyzer;



        public MainWindow()
        {
            InitializeComponent();
            this.peselRadioButton.Checked = true;
        }

        private void checkButton_Click(object sender, EventArgs ea)
        {           

            if (this.nipRadioButton.Checked == true)
            {
                analyzer = new Nip(this.inputTextBox.Text);
            }

            if (this.peselRadioButton.Checked == true)
            {
                analyzer = new Pesel(this.inputTextBox.Text);
            }


            try
            {
                if(analyzer.Analyze())
                {
                    MessageBox.Show("Wpisany numer " + analyzer.GetType().ToString().Remove(0, 7) + " jest poprawny.", "Wynik", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (StringNotValidException ex)
            {
                MessageBox.Show(ex.Description, "Błąd walidacji!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
