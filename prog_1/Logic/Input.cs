﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_1
{
    class Input : IAnalyzer
    {
        protected string value;
        public string Value { get => value; set => this.value = value; }

        public Input(string val)
        {
            this.value = val;
        }

        public virtual bool Analyze()   { return false; }
    }
}
