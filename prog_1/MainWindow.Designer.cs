﻿namespace prog_1
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inputTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.peselRadioButton = new System.Windows.Forms.RadioButton();
            this.nipRadioButton = new System.Windows.Forms.RadioButton();
            this.checkButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // inputTextBox
            // 
            this.inputTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.inputTextBox.Location = new System.Drawing.Point(12, 32);
            this.inputTextBox.MaxLength = 11;
            this.inputTextBox.Name = "inputTextBox";
            this.inputTextBox.Size = new System.Drawing.Size(371, 22);
            this.inputTextBox.TabIndex = 0;
            this.inputTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "PESEL / NIP";
            // 
            // peselRadioButton
            // 
            this.peselRadioButton.AutoSize = true;
            this.peselRadioButton.Location = new System.Drawing.Point(15, 83);
            this.peselRadioButton.Name = "peselRadioButton";
            this.peselRadioButton.Size = new System.Drawing.Size(59, 17);
            this.peselRadioButton.TabIndex = 2;
            this.peselRadioButton.TabStop = true;
            this.peselRadioButton.Text = "PESEL";
            this.peselRadioButton.UseVisualStyleBackColor = true;
            // 
            // nipRadioButton
            // 
            this.nipRadioButton.AutoSize = true;
            this.nipRadioButton.Location = new System.Drawing.Point(15, 106);
            this.nipRadioButton.Name = "nipRadioButton";
            this.nipRadioButton.Size = new System.Drawing.Size(43, 17);
            this.nipRadioButton.TabIndex = 3;
            this.nipRadioButton.TabStop = true;
            this.nipRadioButton.Text = "NIP";
            this.nipRadioButton.UseVisualStyleBackColor = true;
            // 
            // checkButton
            // 
            this.checkButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkButton.Location = new System.Drawing.Point(12, 148);
            this.checkButton.Name = "checkButton";
            this.checkButton.Size = new System.Drawing.Size(371, 38);
            this.checkButton.TabIndex = 4;
            this.checkButton.Text = "Sprawdź";
            this.checkButton.UseVisualStyleBackColor = true;
            this.checkButton.Click += new System.EventHandler(this.checkButton_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 198);
            this.Controls.Add(this.checkButton);
            this.Controls.Add(this.nipRadioButton);
            this.Controls.Add(this.peselRadioButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.inputTextBox);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(411, 236);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(411, 236);
            this.Name = "MainWindow";
            this.Text = "Weryfikator PESEL i NIP";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox inputTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton peselRadioButton;
        private System.Windows.Forms.RadioButton nipRadioButton;
        private System.Windows.Forms.Button checkButton;
    }
}

